package com.example.rafal.facebook;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    CallbackManager cbm;
    AccessToken at;
    Profile p;
    ProfileTracker pt;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.textView);
        cbm = CallbackManager.Factory.create();
        LoginButton lb = (LoginButton) findViewById(R.id.login_button);

        lb.registerCallback(cbm, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                at = loginResult.getAccessToken();
                pt = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        pt.stopTracking();
                        p = Profile.getCurrentProfile();
                        tv.setText("Witaj, " + p.getFirstName() + " " + p.getLastName());
                    }
                };
                pt.startTracking();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Logging in cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Error while logiing in", Toast.LENGTH_SHORT).show();
            }
        });

        ShareLinkContent content = new ShareLinkContent.Builder().setContentUrl(Uri.parse("http://barnburger.pl")).build();

        ShareButton shareButton = (ShareButton) findViewById(R.id.share_button);
        shareButton.setShareContent(content);

        LikeView likeView = (LikeView) findViewById(R.id.like_view);
        likeView.setObjectIdAndType("https://www.facebook.com/barnburger", LikeView.ObjectType.PAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        cbm.onActivityResult(requestCode, resultCode, data);
    }
}